const { codeStem } = require('MFSearch');

console.log (1, codeStem({ code: '123456' }));
console.log (2, codeStem({ code: '123456', level:2 }));
console.log (3, codeStem({ code: '123456', level:3 }));
console.log (4, codeStem({ code: '123456', level:4 }));
console.log (0, codeStem({ code: '123456', level:0 }));

